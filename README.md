# Gatotrade


## Introduction

Simplistic financial exchange. Made by cats.

     /\___/\
    (  o o  )
    /   *   \
    \__\_/__/
      /   \
     / ___ \
     \/___\/

Some of the functionality it provides:

* Create a user on the exchange
* View accounts (which hold the funds for each of the assets that can be traded on the platform)
* Trade assets by creating a limit order (buy or sell) for a trading pair
* View orders in the order book for a trading pair
* View trades for a trading pair

This functionality is currently only available through a REST HTTP API. See the `API` section for more info on the available API methods.

Built with:

* Python 3.7+ (might work on earlier 3.x versions)
* Pyramid
* Swagger for schema validation
* SQLAlchemy (only tested with SQLite for now)
* and some other dependencies (see `setup.py`)


### Disclaimer

This is just a simple proof of concept and not meant to be used in production. Additionally, I have no prior knowledge of financial markets, exchanges etc., so some decisions might be questionable (or plain wrong).

         .-"-.
       _/_-.-_\_
      / __> <__ \
     / //  "  \\ \
    / / \'---'/ \ \
    \ \_/`"""`\_/ /
     \           /


## Quickstart

To install Gatotrade, create and active a virtual environment, then run:

```
pip install -r dev-requirements.txt
```

Before starting the server for the first time, you need to initialise the database:

```
initdb development.ini
```

For testing, you can also create some initial content:

```
populatedb development.ini
```

This will create two products (aka trading pairs): `omg-eth` and `eth-eur`. Additionally, it will create two test users with some funds so you can start trading.

Note that for simplicity we use a SQLite database, which is stored in the `gatotrade.sqlite` file. If you want to start from scratch, just delete this file and run the `initdb` and `populatedb` scripts again.

Then you can start the server:

```
pserve development.ini --reload

```

and start playing with the REST API (see next section for more info).



## API

Gatotrade provides a REST HTTP API for interfacing with the exchange. See `gatotrade/views/schemas` directory which contains Swagger schemas that define API methods that are available (see `gatotrade/views/schemas/gatotrade.yml`) and resources that are accepted or returned by the API.

**TODO**: automatically generate API docs from Swagger schemas


### Public API methods

These API methods do not require any authentication.

#### POST `/users`

Create a user on the exchange by `POST`-ing to this endpoint. Returns the user data and the generated API key ID and secret. You should safely store the API key credentials and provide them when accessing the private API methods (secret cannot be recovered).

NOTE: this endpoint is provided for convenience only, for testing. On a real exchange the user would obviously need to go through a screening process before being allowed on the exchange.

Example request:

```
curl http://localhost:8080/users -X POST -d '{"name": "Grumpy", "surname": "Cat", "email": "grumpy.cat@example.com"}'
```

Example response:

```
{
    "id": "209d8b16-eece-4b3f-aec1-e7c8017def3b",
    "name": "Grumpy",
    "surname": "Cat",
    "email": "grumpy.cat@example.com",
    "bank_accounts": [],
    "apikey": {
        "id": "dG7s6aVV2jl3O4u34TRECE0cYldOmCp4",
        "secret": "25PDyC2u3KK1xJnWdnKAmxGW4jBd0YUrLqQsE3Zy91ikFezywskA2hCwubMWLjXz"
    }
}
```

#### GET `/products/{product_id}/book`

Get the state of the order book for the selected trading pair.

Example request:

```
curl http://localhost:8080/products/omg-eth/book
```

Example response:

```
{
    "buy_orders": [
        {
            "order_id": "2dcb15bc-85d8-4a5f-8efa-434f4e11d66b",
            "price": "10.0000000000",
            "amount": "1.0000000000",
            "status": "new"
        }
    ],
    "sell_orders": [
        {
            "order_id": "225a31fa-ff37-4a59-a3ac-8bbea02aa126",
            "price": "11.0000000000",
            "amount": "2.0000000000",
            "status": "new"
        }
    ]
}
```

#### GET `/products/{product_id}/trades`

Get trades for the selected trading pair.

Example request:

```
curl http://localhost:8080/products/omg-eth/trades
```

Example response:

```
[
    {
        "trade_id": "31fc1393-d488-4367-baa9-2a5136011950",
        "created": "2019-12-10T17:27:20.637217+00:00",
        "amount": "1.0000000000",
        "price": "11.0000000000"
    }
]
```


### Private API methods

These API methods require authentication with API key credentials that were obtained when creating the user (with the `/users` endpoint). Authentication method used is HTTP Basic Authentication; API key ID should be used as the username, and API key secret as the password.

#### GET `/accounts`

Get the state of accounts for the current user.

Example request:

```
curl http://localhost:8080/accounts -H 'Authorization: Basic <TOKEN>'
```

Example response:

```
[
    {
        "account_id": "b8339abc-58a3-4680-8f2a-ba46e9beeed0",
        "asset": "eth",
        "balance": "0.0000000000"
    },
    {
        "account_id": "7f3fbd3e-e2e4-4984-a24a-33e4792a51fb",
        "asset": "eur",
        "balance": "0.0000000000"
    },
    {
        "account_id": "1654d26d-af98-48d5-b065-4173e0bc9c69",
        "asset": "omg",
        "balance": "0.0000000000"
    }
]
```

#### POST `/accounts/{asset_id}/deposits`

Deposit funds to an asset account (e.g. `eur`) by using the selected payment method (e.g. `bank-account`).

NOTE: For now this is just a dummy implementation for testing, which unconditionally adds virtual funds to the asset account

Example request:

```
curl http://localhost:8080/accounts/eur/deposits -X POST -d '{"amount": "1000", "payment_method": "bank-account"}' -H 'Authorization: Basic <TOKEN>'
```

Example response:

```
{
    "account_id": "2ec14e08-a49d-43c7-b721-4007564e5cc6",
    "asset": "eur",
    "balance": "1000.0000000000"
}
```

#### GET `/orders`

Get a list of orders for a user.

Example request:

```
curl http://localhost:8080/orders -H 'Authorization: Basic <TOKEN>'
```

Example response:

```
[
    {
        "order_id": "e8f118d1-3879-47aa-9be5-cdaa46b927ad",
        "product_id": "eth-eur",
        "amount": "10.0000000000",
        "price": "55.1200000000",
        "side": "buy",
        "created": "2019-12-19T21:00:42.117424+00:00",
        "status": "new"
    }
]
```

#### POST `/orders`

Create a new buy or sell limit order by `POST`-ing to this endpoint. Returns order data with the current status of the order. When the order is created, status is `new`. All possible order statuses:

* `new`
* `partially-filled`
* `filled`
* `completed`
* `cancelled`

Example request:

```
curl http://localhost:8080/orders -X POST -d '{"product_id": "eth-eur", "amount": "10", "price": "55.12", "side": "buy"}' -H 'Authorization: Basic <TOKEN>'
```

Example response:


```
{
    "order_id": "143887c5-c408-4545-9bf9-66c721d722ca",
    "product_id": "eth-eur",
    "amount": "10.0000000000",
    "price": "55.1200000000",
    "side": "buy",
    "created": "2019-12-18T23:54:14.358525+00:00",
    "status": "new"
}
```

#### DELETE `/orders/{order_id}`

Cancel an order by sending a `DELETE` HTTP request to this endpoint. Returns order data with the current status of the order (`cancelled`).

Example request:

```
curl http://localhost:8080/orders/143887c5-c408-4545-9bf9-66c721d722ca -X DELETE -H 'Authorization: Basic <TOKEN>'
```

Example response:


```
{
    "order_id": "143887c5-c408-4545-9bf9-66c721d722ca",
    "product_id": "eth-eur",
    "amount": "10.0000000000",
    "price": "55.1200000000",
    "side": "buy",
    "created": "2019-12-18T23:54:14.358525+00:00",
    "status": "cancelled"
}
```


## Development

To install Gatotrade and development libraries, create and active a virtual environment, then run:

```
pip install -r dev-requirements.txt
```

To run all tests and code checks:

```
./test.sh
```


## Design considerations

For simplicity, this solution has the following decisions/limitations:

* Everything (the whole order flow etc.) happens synchronously, in one transaction, tied to the request.
* Only one process and thread (which handles all trading pairs). This is because order book is not thread-safe and order processing needs to happen sequentially. To improve performance, each trading pair should have a separate process.
* Security: we use API key ID and secret, which is stored hashed in the DB. For additional security we could consider implementing HMAC: https://en.wikipedia.org/wiki/HMAC
* It is (obviuosly) missing many features which are needed for an actual working exchange (wallets, connection to payment methods so funds can be deposited on the account, etc.)

For production, it would perhaps make sense to consider the following architecture:

* API service, which accepts orders, stores them in persistent storage, sends an async event that order was created and immediatelly returns a response.
* Matching service, which listens for order events and processes them sequentially, performing matching and recording trades.
* Order service which listens for trade events and updates the orders, account balances etc.
* The client gets order updates via websockets (or by polling order endpoint for updates).
