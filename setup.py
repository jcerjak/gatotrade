from codecs import open
from setuptools import setup
from setuptools import find_packages
import re


def read_file(filepath):
    with open(filepath, 'r', encoding='utf-8') as f:
        content = f.read()

    return content.strip()


def get_dependencies():
    content = read_file('requirements.txt')
    lines = content.split('\n')

    # remove comments
    lines = [re.sub('#.*$', '', line).strip() for line in lines]
    # remove empty lines
    lines = [line for line in lines if line]

    return lines


CLASSIFIERS = [
    'Intended Audience :: Developers',
    'License :: Other/Proprietary License',
    'Operating System :: OS Independent',
    'Topic :: Software Development :: Libraries :: Python Modules',
    'Development Status :: 2 - Pre-Alpha',
    'Programming Language :: Python',
    'Programming Language :: Python :: 2.7',
    'Programming Language :: Python :: 3.4',
]

INSTALL_REQUIRES = [
    'bcrypt',
    'plaster_pastedeploy',
    'pyramid>=1.9a',
    'pyramid_retry',
    'pyramid_tm',
    'sortedcontainers',
    'SQLAlchemy',
    'sqlalchemy-utc',
    'transaction',
    'zope.event',
    'zope.sqlalchemy',

    # For pyramid_swagger
    'pyramid_swagger',
    # TODO: pyramid_swagger depends on jsonschema, but it doesn't specify the
    # format extra, which is required
    'jsonschema[format]',
    'strict-rfc3339',
    'rfc3987',
]

EXTRAS_REQUIRE = {
    'dev': [
        # Pyramid tools
        'pyramid_debugtoolbar',
        'waitress',

        # Testing tools
        'tox',
        'pytest',
        'pytest-mock',
        'WebTest',
        'mock',
        'coverage',
        'flake8',
        'mypy',

        # Security
        'safety',

        # Other
        'ipython',
    ]

}


setup(
    name='gatotrade',
    description='Imaginary simplistic financial exchange. Made by cats.',
    long_description=(
        '%s\n\n%s' % (
            read_file('README.md'),
            read_file('HISTORY.rst'),
        )
    ),
    version='0.1.0dev',
    url='',
    platforms=['OS Independent'],
    classifiers=CLASSIFIERS,
    packages=find_packages(),
    install_requires=INSTALL_REQUIRES,
    extras_require=EXTRAS_REQUIRE,
    entry_points={
        'paste.app_factory': [
            'main = gatotrade:main',
        ],
        'console_scripts': [
            'initdb = gatotrade.scripts.initializedb:main',
            'populatedb = gatotrade.scripts.populatedb:main',
        ],
    },
    include_package_data=True,
    zip_safe=False,
)
