from decimal import Decimal
import datetime
import enum

from sqlalchemy import Column
from sqlalchemy import Enum
from sqlalchemy import ForeignKey
from sqlalchemy import Integer
from sqlalchemy import Numeric
from sqlalchemy.orm import relationship
from sqlalchemy_utc.sqltypes import UtcDateTime
from sqlalchemy_utc.now import utcnow

from gatotrade.models.meta import Base
from gatotrade.models.product import Product
from gatotrade.models.sqltypes import GUID
from gatotrade.models.user import User
from gatotrade.utils import get_utcnow
from gatotrade.utils import generate_uuid


class OrderSide(enum.Enum):
    SELL = 'sell'
    BUY = 'buy'


class OrderStatus(enum.Enum):
    NEW = 'new'
    PARTIALLY_FILLED = 'partially-filled'
    FILLED = 'filled'
    COMPLETED = 'completed'
    CANCELLED = 'cancelled'


class OrderMixin:
    remaining_amount: Decimal

    @property
    def is_filled(self) -> bool:
        return self.status == OrderStatus.FILLED

    @property
    def is_completed(self) -> bool:
        return self.status == OrderStatus.COMPLETED

    @property
    def is_cancelled(self) -> bool:
        return self.status == OrderStatus.CANCELLED

    @property
    def is_active(self) -> bool:
        return not (self.is_filled or self.is_completed or self.is_cancelled)

    def set_status(self, status):
        status = OrderStatus(status)

        # TODO: use a proper workflow/state machine with guard conditions
        if status == OrderStatus.FILLED and self.remaining_amount != 0:
            raise ValueError('Cannot fill order with remaining amount > 0')

        self.status = status

    def cancel(self):
        self.set_status(OrderStatus.CANCELLED)

    def fill(self, amount: Decimal):
        if self.is_filled:
            raise ValueError(f'Order already filled: {self}')

        if amount > self.remaining_amount:
            raise ValueError(
                'Amount "{}" is too big to fill order, remaining '
                'amount: {}'.format(amount, self.remaining_amount))

        self.remaining_amount -= amount
        if self.remaining_amount == 0:
            self.set_status(OrderStatus.FILLED)
        else:
            self.set_status(OrderStatus.PARTIALLY_FILLED)


class Order(Base, OrderMixin):
    __tablename__ = 'orders'

    id = Column(Integer, primary_key=True, nullable=False)
    created = Column(UtcDateTime, default=utcnow(), nullable=False)

    uuid = Column(GUID, nullable=False)
    amount = Column(Numeric, nullable=False)
    remaining_amount = Column(Numeric)
    price = Column(Numeric, nullable=False)
    side = Column(Enum(OrderSide), nullable=False)
    status = Column(Enum(OrderStatus), nullable=False)

    user_id = Column(Integer, ForeignKey('users.id'), nullable=False)
    user = relationship('User', backref='orders')
    product_id = Column(Integer, ForeignKey('products.id'), nullable=False)
    product = relationship('Product', backref='orders')

    def __init__(
        self,
        product: Product,
        amount: Decimal,
        price: Decimal,
        side,
        user: User,
        **kwargs
    ) -> None:
        super(Order, self).__init__(**kwargs)

        self.created = get_utcnow()
        self.uuid = generate_uuid()
        self.amount = amount
        self.remaining_amount = amount
        self.price = price
        self.side = OrderSide(side)
        self.set_status(OrderStatus.NEW)

        self.user = user
        self.product = product

    def __repr__(self) -> str:
        return (
            '<{cls} side={side} price={price} status={status} '
            'created={created}>').format(
                cls=self.__class__.__name__,
                side=self.side,
                status=self.status,
                price=self.price,
                created=self.created,
            )

    def to_dict(self):
        return {
            'order_id': str(self.uuid),
            'product_id': self.product.product_id,
            'amount': self.amount,
            'price': self.price,
            'side': self.side.value,
            'created': self.created,
            'status': self.status.value,
        }


class LimitOrder(OrderMixin):
    """Non-persistent limit order for usage in the limit order book.
    """
    def __init__(
        self,
        uuid: str,
        created: datetime.datetime,
        amount: Decimal,
        price: Decimal,
        side,
        status: str,
    ) -> None:
        self.uuid = uuid
        self.created = created
        self.amount = amount
        self.remaining_amount = amount
        self.price = price
        self.side = OrderSide(side)
        self.set_status(status)

    def __lt__(self, other: 'LimitOrder') -> bool:
        if not isinstance(other, LimitOrder):
            raise TypeError(
                'Unorderable types: {} and {}'.format(type(self), type(other)))

        if self.price < other.price:
            return True
        elif self.price == other.price and self.created < other.created:
            return True
        return False

    def __gt__(self, other: 'LimitOrder') -> bool:
        return not self.__lt__(other)

    def __repr__(self) -> str:
        return (
            '<{cls} side={side} price={price} status={status} '
            'created={created}>').format(
                cls=self.__class__.__name__,
                side=self.side,
                status=self.status,
                price=self.price,
                created=self.created,
            )

    @classmethod
    def from_order(cls, order: Order):
        return cls(
            uuid=order.uuid,
            created=order.created,
            amount=order.amount,
            price=order.price,
            side=order.side,
            status=order.status,
        )

    def to_dict(self):
        return {
            'order_id': str(self.uuid),
            'created': self.created,
            'amount': self.amount,
            'remaining_amount': self.remaining_amount,
            'price': self.price,
            'side': self.side.value,
            'status': self.status.value,
        }
