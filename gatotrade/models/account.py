from decimal import Decimal

from sqlalchemy import Column
from sqlalchemy import Enum
from sqlalchemy import ForeignKey
from sqlalchemy import Integer
from sqlalchemy import Numeric
from sqlalchemy import Unicode
from sqlalchemy.schema import UniqueConstraint
from sqlalchemy_utc.now import utcnow
from sqlalchemy_utc.sqltypes import UtcDateTime

from gatotrade.exceptions import AmountTooBigException
from gatotrade.models.meta import Base
from gatotrade.models.product import Asset
from gatotrade.models.sqltypes import GUID
from gatotrade.utils import generate_uuid
from gatotrade.utils import get_utcnow


class BankAccount(Base):
    __tablename__ = 'bank_accounts'

    id = Column(Integer, primary_key=True, nullable=False)
    created = Column(UtcDateTime, default=utcnow(), nullable=False)

    uuid = Column(GUID, nullable=False)
    holder_name = Column(Unicode, nullable=False)
    currency = Column(Unicode, nullable=False)
    iban = Column(Unicode, nullable=False)
    bic = Column(Unicode, nullable=False)

    # foreign keys
    user_id = Column(Integer, ForeignKey('users.id'), nullable=False)

    def __init__(
        self,
        holder_name: str,
        currency: str,
        iban: str,
        bic: str,
        user,
        **kwargs
    ) -> None:
        super(BankAccount, self).__init__(**kwargs)

        self.created = get_utcnow()
        self.uuid = generate_uuid()
        self.holder_name = holder_name
        self.currency = currency
        self.iban = iban
        self.bic = bic
        self.user = user


class Account(Base):
    """Represents an account for a specific asset and user.
    """
    __tablename__ = 'accounts'

    id = Column(Integer, primary_key=True, nullable=False)
    created = Column(UtcDateTime, default=utcnow(), nullable=False)

    uuid = Column(GUID, nullable=False)
    asset = Column(Enum(Asset), nullable=False)
    _balance = Column('balance', Numeric, nullable=False)

    # foreign keys
    user_id = Column(Integer, ForeignKey('users.id'), nullable=False)
    # user = relationship('User', backref='accounts')

    __table_args__ = (
        UniqueConstraint('user_id', 'asset'),
    )

    def __init__(self, asset, user, **kwargs):
        super(Account, self).__init__(**kwargs)

        self.created = get_utcnow()
        self.uuid = generate_uuid()
        self.asset = Asset(asset)
        self.user = user

        self._balance = Decimal('0')

    @property
    def balance(self):
        return self._balance

    def deposit(self, amount):
        self._balance += amount

    def withdraw(self, amount):
        # TODO: take fees into account as well!
        if amount > self._balance:
            raise AmountTooBigException(amount)

        self._balance -= amount

    def to_dict(self):
        return {
            'account_id': str(self.uuid),
            'asset': self.asset.value,
            'balance': self.balance,
        }
