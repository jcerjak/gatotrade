import enum
from decimal import Decimal

from sqlalchemy import Column
from sqlalchemy import Enum
from sqlalchemy import Integer
from sqlalchemy import Numeric
from sqlalchemy import Unicode
from sqlalchemy_utc.sqltypes import UtcDateTime
from sqlalchemy_utc.now import utcnow

from gatotrade.models.meta import Base
from gatotrade.utils import get_utcnow


class Asset(enum.Enum):
    omg = 'omg'
    eth = 'eth'
    eur = 'eur'


class Product(Base):
    __tablename__ = 'products'

    id = Column(Integer, primary_key=True, nullable=False)
    created = Column(UtcDateTime, default=utcnow(), nullable=False)

    product_id = Column(Unicode, nullable=False, unique=True)
    base_asset = Column(Enum(Asset), nullable=False)
    quote_asset = Column(Enum(Asset), nullable=False)
    min_amount = Column(Numeric, nullable=False)
    max_amount = Column(Numeric, nullable=False)

    def __init__(
        self,
        product_id: str,
        base_asset,
        quote_asset,
        min_amount: Decimal,
        max_amount: Decimal,
        **kwargs,
    ) -> None:
        super(Product, self).__init__(**kwargs)

        self.created = get_utcnow()
        self.product_id = product_id
        self.base_asset = Asset(base_asset)
        self.quote_asset = Asset(quote_asset)
        self.min_amount = min_amount
        self.max_amount = max_amount


def get_available_assets():
    return list(Asset)
