import dataclasses
import enum
import typing

from sqlalchemy import Column
from sqlalchemy import Enum
from sqlalchemy import ForeignKey
from sqlalchemy import Integer
from sqlalchemy import Unicode
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import relationship
from sqlalchemy_utc.now import utcnow
from sqlalchemy_utc.sqltypes import UtcDateTime
import bcrypt

from gatotrade.exceptions import AssetNotFoundException
from gatotrade.models.account import Account
from gatotrade.models.account import BankAccount
from gatotrade.models.meta import Base
from gatotrade.models.product import get_available_assets
from gatotrade.models.sqltypes import GUID
from gatotrade.utils import generate_secret
from gatotrade.utils import generate_uuid
from gatotrade.utils import get_utcnow


class UserStatus(enum.Enum):
    NEW = 'new'
    ACTIVE = 'active'
    INACTIVE = 'inactive'


class ApiKeyStatus(enum.Enum):
    ACTIVE = 'active'
    INACTIVE = 'inactive'
    DISABLED = 'disabled'


class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True, nullable=False)
    created = Column(UtcDateTime, default=utcnow(), nullable=False)

    uuid = Column(GUID, nullable=False)
    name = Column(Unicode, nullable=False)
    surname = Column(Unicode, nullable=False)
    email = Column(Unicode, nullable=False, unique=True)
    _status = Column(Enum(UserStatus), nullable=False)

    bank_accounts = relationship('BankAccount', backref='user')
    accounts = relationship('Account', backref='user')
    apikey = relationship('ApiKey', backref='user', uselist=False)

    def __init__(
        self,
        name: str,
        surname: str,
        email: str,
        bank_accounts: typing.List[BankAccount] = None,
        accounts: typing.List[Account] = None,
        generate_apikey: bool = False,
        generate_accounts: bool = False,
        **kwargs
    ) -> None:
        super(User, self).__init__(**kwargs)

        self.created = get_utcnow()
        # TODO: let DB generate uuid?
        self.uuid = generate_uuid()
        self.name = name
        self.surname = surname
        self.email = email
        self.set_status(UserStatus.NEW)
        self.bank_accounts = bank_accounts or []

        self.accounts = []
        if generate_accounts:
            for asset in get_available_assets():
                account = Account(asset=asset, user=self)
                self.accounts.append(account)

        self.apikey = None
        if generate_apikey:
            self.apikey = ApiKey(user=self)

    @hybrid_property
    def status(self):
        return self._status

    def set_status(self, status):
        self._status = UserStatus(status)

    def activate(self):
        self.set_status(UserStatus.ACTIVE)

    def is_active(self):
        return self.status == UserStatus.ACTIVE

    def get_account_by_asset(self, asset):
        for account in self.accounts:
            if account.asset == asset:
                return account
        raise AssetNotFoundException

    def to_dict(self):
        return {
            'id': str(self.uuid),
            'name': self.name,
            'surname': self.surname,
            'email': self.email,
            'bank_accounts': self.bank_accounts,
        }


@dataclasses.dataclass
class ApiKeyPair:
    id: str
    secret: str


class ApiKey(Base):
    __tablename__ = 'apikeys'

    id = Column(Integer, primary_key=True, nullable=False)
    created = Column(UtcDateTime, default=utcnow(), nullable=False)

    apikey_id = Column(Unicode, nullable=False)
    secret = Column(Unicode, nullable=False)
    _status = Column(Enum(ApiKeyStatus), nullable=False)

    user_id = Column(Integer, ForeignKey('users.id'), nullable=False)

    def __init__(
            self,
            user: User,
            apikey_pair: ApiKeyPair = None,
            **kwargs
    ) -> None:
        super(ApiKey, self).__init__(**kwargs)

        self.created = get_utcnow()
        self.user = user
        self.set_status(ApiKeyStatus.ACTIVE)

        if apikey_pair is None:
            apikey_pair = self.generate_apikey_pair()
            self.apikey_id = apikey_pair.id
            self.secret = self.hash_secret(apikey_pair.secret)
        else:
            self.apikey_id = apikey_pair.id
            self.secret = self.hash_secret(apikey_pair.secret)

    @hybrid_property
    def status(self):
        return self._status

    @staticmethod
    def generate_apikey_pair() -> ApiKeyPair:
        """Generate API key id and secret. The secret should be hashed before
        being stored.
        """
        apikey_pair = ApiKeyPair(
            id=generate_secret(32), secret=generate_secret(64))
        return apikey_pair

    def set_status(self, status):
        self._status = ApiKeyStatus(status)

    def is_active(self):
        return self.status == ApiKeyStatus.ACTIVE

    def hash_secret(self, secret: str) -> str:
        hashed = bcrypt.hashpw(secret.encode('utf-8'), bcrypt.gensalt())
        return hashed.decode('utf-8')

    def is_secret_valid(self, secret: str) -> bool:
        return bcrypt.checkpw(
            secret.encode('utf-8'), self.secret.encode('utf-8'))
