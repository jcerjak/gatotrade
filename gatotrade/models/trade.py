from decimal import Decimal
from uuid import UUID
import dataclasses
import datetime

from sqlalchemy import Column
from sqlalchemy import ForeignKey
from sqlalchemy import Integer
from sqlalchemy import Numeric
from sqlalchemy.orm import relationship
from sqlalchemy_utc.sqltypes import UtcDateTime
from sqlalchemy_utc.now import utcnow

from gatotrade.models.order import LimitOrder
from gatotrade.models.order import Order
from gatotrade.models.meta import Base
from gatotrade.models.sqltypes import GUID
from gatotrade.utils import generate_id
from gatotrade.utils import get_utcnow


class Trade(Base):
    __tablename__ = 'trades'

    id = Column(Integer, primary_key=True, nullable=False)
    created = Column(UtcDateTime, default=utcnow(), nullable=False)

    uuid = Column(GUID, nullable=False)
    amount = Column(Numeric, nullable=False)
    price = Column(Numeric, nullable=False)

    product_id = Column(Integer, ForeignKey('products.id'), nullable=False)
    product = relationship('Product', backref='trades')
    buy_order_id = Column(Integer, ForeignKey('orders.id'), nullable=False)
    buy_order = relationship(
        'Order', foreign_keys=[buy_order_id], backref='buy_trades')
    sell_order_id = Column(Integer, ForeignKey('orders.id'), nullable=False)
    sell_order = relationship(
        'Order', foreign_keys=[sell_order_id], backref='sell_trades')

    def __init__(
        self,
        amount: Decimal,
        price: Decimal,
        buy_order: Order,
        sell_order: Order,
        **kwargs,
    ) -> None:
        super(Trade, self).__init__(**kwargs)

        self.created = get_utcnow()
        self.uuid = generate_id()
        self.amount = amount
        self.price = price
        self.buy_order = buy_order
        self.sell_order = sell_order
        assert buy_order.product == sell_order.product
        self.product = buy_order.product

    def to_dict(self):
        return {
            'trade_id': self.uuid,
            'created': self.created,
            'amount': self.amount,
            'price': self.price,
        }


@dataclasses.dataclass
class LimitOrderTrade:
    """Non-persistent trade for usage in the limit order book.
    """
    amount: Decimal
    price: Decimal
    buy_order: LimitOrder
    sell_order: LimitOrder
    uuid: UUID = generate_id()
    created: datetime.datetime = get_utcnow()
