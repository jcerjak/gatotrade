from pyramid.security import Allow

from gatotrade.exceptions import UserNotFoundException
from gatotrade.users import UserManager


class Permissions:
    get_own_orders = 'get_own_orders'
    create_order = 'create_order'
    cancel_order = 'cancel_order'
    get_own_accounts = 'get_own_accounts'
    deposit_to_account = 'deposit_to_account'


class Groups:
    USER = 'g:user'


class RootFactory:
    __acl__ = [
        (Allow, Groups.USER, Permissions.get_own_orders),
        (Allow, Groups.USER, Permissions.create_order),
        (Allow, Groups.USER, Permissions.cancel_order),
        (Allow, Groups.USER, Permissions.get_own_accounts),
        (Allow, Groups.USER, Permissions.deposit_to_account),
    ]

    def __init__(self, request):
        self.request = request


def check_auth(username, password, request):
    users = UserManager(request.dbsession, request.registry)

    try:
        user = users.get_by_apikey_id(username)
        if not user.apikey.is_secret_valid(password):
            raise UserNotFoundException
    except UserNotFoundException:
        user = None

    if user and user.is_active() and user.apikey.is_active():
        return [Groups.USER]
    return None
