from operator import neg
import typing

from sortedcontainers import SortedDict

from gatotrade.models.order import LimitOrder
from gatotrade.models.order import OrderSide
from gatotrade.models.trade import LimitOrderTrade


class LimitOrderBook:
    """Limit order book based on "price time priority".

    We keep a list of orders sorted by time (oldest first) and grouped by price
    point. Price points for buy orders are sorted from high to low, while price
    points for sell orders are sorted from low to high.
    """
    def __init__(self):
        self.buy_orders = SortedDict(neg)
        self.sell_orders = SortedDict()
        self.orders_by_uuid = {}
        self.trades: typing.List[LimitOrderTrade] = []

    def add_order(self, order: LimitOrder) -> typing.Sequence[LimitOrderTrade]:
        if order.side == OrderSide.BUY:
            pricepoint_orders = self.buy_orders.setdefault(order.price, [])
            pricepoint_orders.append(order)
        elif order.side == OrderSide.SELL:
            pricepoint_orders = self.sell_orders.setdefault(order.price, [])
            pricepoint_orders.append(order)
        else:  # sanity check
            raise ValueError(f'Invalid order side: {order.side}')

        self.orders_by_uuid[order.uuid] = order
        trades = self._try_fill_orders()
        return trades

    def cancel_order(self, order: LimitOrder):
        # for now we still keep cancelled orders in the orderbook for
        # simplicity, we just update the order status
        assert order.is_cancelled
        self.orders_by_uuid[order.uuid].status = order.status

    def to_dict(self, skip_inactive_orders=True):
        buy_orders = []
        for price_point, orders in self.buy_orders.items():
            for order in orders:
                if not order.is_active and skip_inactive_orders:
                    continue
                buy_order = {
                    'order_id': str(order.uuid),
                    'price': order.price,
                    'amount': order.amount,
                    'remaining_amount': order.remaining_amount,
                    'status': order.status.value,
                }
                buy_orders.append(buy_order)

        sell_orders = []
        for price_point, orders in self.sell_orders.items():
            for order in orders:
                if not order.is_active and skip_inactive_orders:
                    continue
                sell_order = {
                    'order_id': str(order.uuid),
                    'price': order.price,
                    'amount': order.amount,
                    'remaining_amount': order.remaining_amount,
                    'status': order.status.value,
                }
                sell_orders.append(sell_order)

        return {
            'buy_orders': buy_orders,
            'sell_orders': sell_orders,
        }

    def _try_fill_orders(self):
        trades = []

        # TODO: clear non-active orders?
        for price_point, pp_buy_orders in self.buy_orders.items():
            for buy_order in pp_buy_orders:
                if buy_order.is_active:
                    trades_for_buy_order = self._fill_buy_order(buy_order)
                    trades.extend(trades_for_buy_order)

                    # order was not fully filled, matching not possible anymore
                    if not buy_order.is_filled:
                        return trades

        return trades

    def _fill_buy_order(self, buy_order: LimitOrder):
        trades = []
        for sell_price_point, sell_orders in self.sell_orders.items():
            # cannot make a trade anymore, selling price is higher than buying
            if sell_price_point > buy_order.price:
                break

            for sell_order in sell_orders:
                if sell_order.is_active:
                    trade = self._make_trade(
                        buy_order=buy_order, sell_order=sell_order)
                    trades.append(trade)
                    if buy_order.is_filled:
                        return trades

        return trades

    def _make_trade(
        self,
        buy_order: LimitOrder,
        sell_order: LimitOrder
    ) -> LimitOrderTrade:
        assert buy_order.is_active
        assert sell_order.is_active

        amount = min(
            buy_order.remaining_amount,
            sell_order.remaining_amount
        )
        price = sell_order.price
        buy_order.fill(amount)
        sell_order.fill(amount)

        limit_order_trade = LimitOrderTrade(
            amount=amount,
            price=price,
            buy_order=buy_order,
            sell_order=sell_order,
        )
        self.trades.append(limit_order_trade)

        return limit_order_trade
