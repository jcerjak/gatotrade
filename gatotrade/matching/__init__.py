import transaction

from gatotrade.models import get_session_factory
from gatotrade.models import get_tm_session

from .orderbook_manager import LimitOrderBookManager


def init_limit_order_books(config) -> LimitOrderBookManager:
    session_factory = get_session_factory()
    with transaction.manager:
        dbsession = get_tm_session(session_factory, transaction.manager)
        limit_order_books = LimitOrderBookManager()
        limit_order_books.init_order_books(dbsession)

    return limit_order_books
