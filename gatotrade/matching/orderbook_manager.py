from gatotrade.exceptions import InvalidProductIdException
from gatotrade.models import Order
from gatotrade.models import Product
from gatotrade.models.order import LimitOrder
from gatotrade.models.order import OrderStatus

from .orderbook import LimitOrderBook


class LimitOrderBookManager:
    INCOMPLETE_ORDER_STATUSES = {
        OrderStatus.NEW,
        OrderStatus.PARTIALLY_FILLED,
    }

    def __init__(self):
        self._limit_order_books = {}

    def get_book(self, product_id: str):
        try:
            return self._limit_order_books[product_id]
        except KeyError:
            raise InvalidProductIdException(product_id)

    def set_book(self, product_id: str, limit_order_book: LimitOrderBook):
        try:
            self._limit_order_books[product_id] = limit_order_book
        except KeyError:
            raise InvalidProductIdException(product_id)

    def init_order_books(self, dbsession):
        for product in dbsession.query(Product).all():
            self._limit_order_books[product.product_id] = LimitOrderBook()

        incomplete_orders = (
            dbsession.query(Order).
            filter(Order.status.in_(self.INCOMPLETE_ORDER_STATUSES)).all())
        for order in incomplete_orders:
            limit_order = LimitOrder.from_order(order)
            limit_order_book = self.get_book(order.product.product_id)
            limit_order_book.add_order(limit_order)
