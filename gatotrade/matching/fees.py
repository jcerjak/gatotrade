from decimal import Decimal

from gatotrade.models.trade import Trade


FIXED_FEE_PERCENTAGE = Decimal('0.001')  # 0.10 %


class AbstractFeePolicy:
    def get_buyer_fees(self, trade: Trade):
        raise NotImplementedError

    def get_seller_fees(self, trade: Trade):
        raise NotImplementedError


class FixedFeePolicy(AbstractFeePolicy):
    def __init__(self, fee_percentage: Decimal) -> None:
        self.fee_percentage = fee_percentage

    def get_buyer_fees(self, trade: Trade):
        fees = trade.amount * self.fee_percentage

        return fees

    def get_seller_fees(self, trade: Trade):
        fees = trade.amount * trade.price * self.fee_percentage

        return fees


fixed_fee_policy = FixedFeePolicy(FIXED_FEE_PERCENTAGE)


def get_fee_policy():
    return fixed_fee_policy
