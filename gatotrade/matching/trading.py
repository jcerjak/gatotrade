import logging
from gatotrade.events import TradeCreatedEvent
from gatotrade.exceptions import TradeWithSelfException
from gatotrade.models.trade import LimitOrderTrade
from gatotrade.models.trade import Trade

from .fees import get_fee_policy


logger = logging.getLogger(__name__)


class TradeProcessor:
    """Process the trade and update balances.

    Example:

    Trade ETH/EUR, with amount 10 ETH and price 500 EUR/ETH.
    Fees 0.10% (0.01 ETH or 5 EUR for this trade).

    Buyer: debit 5000 EUR, credit 9.99 ETH
    Seller: debit 10 ETH, credit 4995 EUR
    """
    def __init__(self, dbsession, registry, order_manager) -> None:
        self.dbsession = dbsession
        self.registry = registry
        self.order_manager = order_manager

    def process_trade(self, limit_order_trade: LimitOrderTrade):
        logger.info('Start processing trade: %s', limit_order_trade)
        trade = self._create_trade(limit_order_trade)
        self._process_buy_order(trade)
        self._process_sell_order(trade)

        self.registry.notify(TradeCreatedEvent(trade.uuid))

        return trade

    def _create_trade(self, trade: LimitOrderTrade) -> Trade:
        buy_order = self.order_manager.get_by_uuid(trade.buy_order.uuid)
        sell_order = self.order_manager.get_by_uuid(trade.sell_order.uuid)
        if buy_order.user == sell_order.user:
            raise TradeWithSelfException(trade)

        trade = Trade(
            amount=trade.amount,
            price=trade.price,
            buy_order=buy_order,
            sell_order=sell_order
        )
        self.dbsession.add(trade)

        return trade

    def _process_buy_order(self, trade: Trade):
        # fill the order
        buy_order = trade.buy_order
        buy_order.fill(trade.amount)

        # update accounts
        user = buy_order.user
        account_from = user.get_account_by_asset(buy_order.product.quote_asset)
        account_to = user.get_account_by_asset(buy_order.product.base_asset)
        fee_policy = get_fee_policy()
        # TODO: transfer the fees to the exchange account
        fees = fee_policy.get_buyer_fees(trade)
        amount_to_debit = trade.amount * trade.price
        amount_to_credit = trade.amount - fees
        account_from.withdraw(amount_to_debit)
        account_to.deposit(amount_to_credit)

    def _process_sell_order(self, trade: Trade):
        # fill the order
        sell_order = trade.sell_order
        sell_order.fill(trade.amount)

        # update accounts
        user = sell_order.user
        account_from = user.get_account_by_asset(sell_order.product.base_asset)
        account_to = user.get_account_by_asset(sell_order.product.quote_asset)

        fee_policy = get_fee_policy()
        # TODO: transfer the fees to the exchange account
        fees = fee_policy.get_seller_fees(trade)
        amount_to_debit = trade.amount
        amount_to_credit = trade.amount * trade.price - fees
        account_from.withdraw(amount_to_debit)
        account_to.deposit(amount_to_credit)
