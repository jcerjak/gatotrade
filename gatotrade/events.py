from gatotrade.utils import get_timestamp


class AbstractEvent:
    """Base class for all events."""


class OrderCreatedEvent(AbstractEvent):
    def __init__(self, order_uuid):
        self.order_uuid = order_uuid
        self.timestamp = get_timestamp()


class TradeCreatedEvent(AbstractEvent):
    def __init__(self, trade_uuid):
        self.trade = trade_uuid
        self.timestamp = get_timestamp()


class UserCreatedEvent(AbstractEvent):
    def __init__(self, user):
        self.user = user
        self.timestamp = get_timestamp()
