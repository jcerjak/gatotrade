import logging

from pyramid.authentication import BasicAuthAuthenticationPolicy
from pyramid.authorization import ACLAuthorizationPolicy
from pyramid.config import Configurator
from pyramid.renderers import JSON
from pyramid.settings import asbool
from pyramid_swagger.renderer import PyramidSwaggerRendererFactory

from gatotrade.auth import check_auth
from gatotrade.auth import RootFactory
from gatotrade.matching import init_limit_order_books
from gatotrade.views.serialize import swagger_user_formats


logging.basicConfig(level=logging.INFO)


def configure(global_config, **settings) -> Configurator:
    settings['pretty_print_json'] = asbool(
        settings.get('pretty_print_json', 'false'))
    settings['pyramid_swagger.user_formats'] = swagger_user_formats

    with Configurator(settings=settings) as config:
        config.include('.views')
        config.include('.models')
        config.scan()

        # authentication/authorization config
        authentication_policy = BasicAuthAuthenticationPolicy(
            check=check_auth,
            realm='Gatotrade',
        )
        authorization_policy = ACLAuthorizationPolicy()
        config.set_authentication_policy(authentication_policy)
        config.set_authorization_policy(authorization_policy)
        config.set_root_factory(RootFactory)

        # configure renderers
        if settings.get('pretty_print_json'):
            json_renderer_factory = JSON(indent=4)
        else:
            json_renderer_factory = JSON()
        config.add_renderer(
            name='pyramid_swagger',
            factory=PyramidSwaggerRendererFactory(json_renderer_factory))

    return config


def initdb(config: Configurator):
    config.registry.limit_order_books = init_limit_order_books(config)


def main(global_config, **settings):
    """Return a WSGI application."""
    config = configure(global_config, **settings)
    initdb(config)
    app = config.make_wsgi_app()
    return app
