import typing

from gatotrade.events import UserCreatedEvent
from gatotrade.exceptions import UserExistsException
from gatotrade.exceptions import UserNotFoundException
from gatotrade.models import ApiKey
from gatotrade.models import User
from gatotrade.models.user import ApiKeyPair


class UserManager:
    def __init__(self, dbsession, registry):
        self.dbsession = dbsession
        self.registry = registry

    def add_user(self, data: typing.Mapping) -> typing.Tuple[
            User, ApiKeyPair]:
        # TODO: use an index for email
        user = self.dbsession.query(User).filter(
            User.email == data['email']).first()
        if user:
            raise UserExistsException('User with this email already exists!')

        # TODO: currently we immediately activate users without any screening
        user = User(generate_accounts=True, **data)
        apikey_pair = ApiKey.generate_apikey_pair()
        user.apikey = ApiKey(user=user, apikey_pair=apikey_pair)
        user.activate()
        self.dbsession.add(user)

        self.registry.notify(UserCreatedEvent(user))

        return user, apikey_pair

    def get_by_email(self, email):
        # TODO: use an index for email
        user = self.dbsession.query(User).filter(
            User.email == email).one_or_none()
        if user is None:
            raise UserNotFoundException()

        return user

    def get_by_apikey_id(self, apikey_id):
        # TODO: use an index for apikey_id?
        user = self.dbsession.query(User).join(ApiKey).filter(
            ApiKey.apikey_id == apikey_id).one_or_none()
        if user is None:
            raise UserNotFoundException()

        return user


def get_user_from_request(request) -> User:
    users = UserManager(request.dbsession, request.registry)
    user = users.get_by_apikey_id(request.authenticated_userid)
    return user
