import copy

from gatotrade.events import OrderCreatedEvent
from gatotrade.exceptions import AmountTooBigException
from gatotrade.exceptions import AmountTooSmallException
from gatotrade.exceptions import CannotCancelOrderException
from gatotrade.exceptions import InvalidProductIdException
from gatotrade.exceptions import OrderNotFoundException
from gatotrade.matching.trading import TradeProcessor
from gatotrade.models import Order
from gatotrade.models import Product
from gatotrade.models import User
from gatotrade.models.order import LimitOrder
from gatotrade.models.order import OrderSide


class OrderCreatePolicy:
    def validate_order(self, order: Order):
        """Policy which determines if the user can place an order. It raises an
        `CannotPlaceOrderException` exception if the order cannot be placed.

        For now this only does basic checks if the user has enough funds. But
        we could add additional checks, and maybe have a different policy for
        each product.
        """
        # this is already checked when performing auth, but let's do a sanity
        # check
        assert order.user.is_active()

        if order.side == OrderSide.SELL:
            account = order.user.get_account_by_asset(order.product.base_asset)
            if account.balance < order.amount:
                raise AmountTooBigException(order.amount)
        elif order.side == OrderSide.BUY:
            account = order.user.get_account_by_asset(
                order.product.quote_asset)
            if account.balance < order.amount * order.price:
                raise AmountTooBigException(order.amount)
        else:  # sanity check
            raise ValueError(f'Invalid order side: {order.side}')

        if order.amount < order.product.min_amount:
            raise AmountTooSmallException(order.amount)
        elif order.amount > order.product.max_amount:
            raise AmountTooBigException(order.amount)


class OrderManager:
    def __init__(self, dbsession, registry):
        self.dbsession = dbsession
        self.registry = registry
        self.order_create_policy = OrderCreatePolicy()

    def add_order(self, data, user, product_id):
        """Add the order to the database and to the order book, and immediatelly
        execute the trades (if any).
        """
        # TODO: add index for product_id
        product = self.dbsession.query(Product).filter(
            Product.product_id == product_id).one_or_none()
        if product is None:
            raise InvalidProductIdException(product_id)

        order = Order(product=product, user=user, **data)
        self.order_create_policy.validate_order(order)
        self.dbsession.add(order)
        limit_order_books = self.registry.limit_order_books
        orderbook = limit_order_books.get_book(product_id)
        orderbook_copy = copy.deepcopy(orderbook)
        limit_order = LimitOrder.from_order(order)

        try:
            trades = orderbook.add_order(limit_order)
            trade_processor = TradeProcessor(
                self.dbsession, self.registry, self)
            for trade in trades:
                trade_processor.process_trade(trade)
            self.registry.notify(OrderCreatedEvent(order.uuid))
        except Exception:
            # Any changes to orders, trades etc. in the DB will be rolled back
            # by `pyramid_tm` machinery, but it's possible that the in-memory
            # order book is now corrupted, so let's re-initialize it.

            # FIXME: This is quite messy... If we want to keep a similar
            # approach, it would be cleaner to write a custom transaction data
            # manager, which would join the transaction and rollback in case
            # the transaction is aborted (maybe just by copying the order book
            # on transaction start and reset it on abort). For more info see:
            # see https://zodb.readthedocs.io/en/latest/transactions.html#writing-our-own-data-manager  # noqa
            # Or maybe just do the orderbook in DB with some smart caching...
            self.registry.limit_order_books.set_book(
                product_id, orderbook_copy)
            raise

        return order

    def get_by_uuid(self, order_uuid: str):
        # TODO: use index for uuid
        order = self.dbsession.query(Order).filter(
            Order.uuid == order_uuid).one_or_none()
        if order is None:
            raise OrderNotFoundException(order_uuid)

        return order

    def get_orders_for_user(self, user: User):
        orders = self.dbsession.query(Order).filter(
            Order.user == user).all()
        return orders

    def cancel_order_for_user(self, user: User, order_uuid: str):
        order = self.get_by_uuid(order_uuid)
        if order.user != user:
            raise OrderNotFoundException(order_uuid)
        if not order.is_active:
            raise CannotCancelOrderException(order_uuid)

        order.cancel()

        limit_order_books = self.registry.limit_order_books
        orderbook = limit_order_books.get_book(order.product.product_id)
        limit_order = LimitOrder.from_order(order)
        orderbook.cancel_order(limit_order)

        return order
