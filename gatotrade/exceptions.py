class GatoTradeException(Exception):
    """Base class for all exceptions."""


class CannotPlaceOrderException(GatoTradeException):
    pass


class CannotCancelOrderException(GatoTradeException):
    pass


class AmountTooBigException(CannotPlaceOrderException):
    pass


class AmountTooSmallException(CannotPlaceOrderException):
    pass


class InvalidProductIdException(GatoTradeException):
    pass


class OrderNotFoundException(GatoTradeException):
    pass


class UserExistsException(GatoTradeException):
    pass


class UserNotFoundException(GatoTradeException):
    pass


class AssetNotFoundException(GatoTradeException):
    pass


class TradeWithSelfException(GatoTradeException):
    pass
