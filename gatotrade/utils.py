import datetime
import secrets
import string
import uuid


DEFAULT_SECRETS_ALPHABET = string.ascii_letters + string.digits
MIN_SECRET_LENGTH = 32


def get_utcnow():
    now = datetime.datetime.utcnow()
    now = now.replace(tzinfo=datetime.timezone.utc)
    return now


def get_timestamp():
    return get_utcnow().isoformat()


def generate_uuid():
    return uuid.uuid4()


def generate_id():
    return str(uuid.uuid4())


def generate_secret(length: int, alphabet: str = None):
    """Generate a secret of given length with the chosen alphabet.

    If the alphabet is not provided, default one is used, which
    contains ASCII chars and digits.

    See:
    https://docs.python.org/3/library/secrets.html#recipes-and-best-practices
    """
    if length < MIN_SECRET_LENGTH:
        raise ValueError(
            'Secret length needs to be equal or higher than {}'.format(
                MIN_SECRET_LENGTH))

    if alphabet is None:
        alphabet = DEFAULT_SECRETS_ALPHABET

    secret = ''.join(secrets.choice(alphabet) for _ in range(length))
    return secret
