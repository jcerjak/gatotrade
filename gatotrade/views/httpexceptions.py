from pyramid.httpexceptions import HTTPBadRequest
from pyramid.httpexceptions import HTTPConflict
from pyramid.httpexceptions import HTTPNotFound


# Client errors
# TODO: make error message more specific

class InvalidProductIdHTTPError(HTTPNotFound):
    explanation = 'Provided product id not found'


class InvalidOrderIdHTTPError(HTTPNotFound):
    explanation = 'Provided order id not found'


class UserExistsHTTPError(HTTPBadRequest):
    explanation = 'User with the provided email already exists'


class CannotPlaceOrderHTTPError(HTTPBadRequest):
    explanation = 'Cannot place order, insufficient funds'


class CannotCancelOrderHTTPError(HTTPConflict):
    explanation = 'Cannot cancel order in this state'


class TradeWithSelfHTTPError(HTTPConflict):
    explanation = 'Order aborted, cannot trade with self'


class InvalidAssetIdHTTPError(HTTPNotFound):
    explanation = 'Provided asset id is not valid'
