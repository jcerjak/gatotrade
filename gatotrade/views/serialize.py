"""Custom serialization formats for Swagger schemas."""
import decimal
import uuid

from bravado_core.exception import SwaggerValidationError
from pyramid_swagger.tween import SwaggerFormat


def uuid_str_to_python(value: str):
    try:
        return uuid.UUID(value)
    except ValueError:
        raise SwaggerValidationError('Invalid uuid: {}'.format(value))


def uuid_to_wire(value: uuid.UUID):
    return str(value)


def decimal_str_to_python(value: str):
    # TODO: maybe limit accuracy?
    try:
        return decimal.Decimal(value)
    except decimal.InvalidOperation:
        raise SwaggerValidationError('Invalid decimal: {}'.format(value))


def decimal_to_wire(value: decimal.Decimal):
    # TODO: check if we're loosing accuracy here
    return '{0:f}'.format(value)


price_format = SwaggerFormat(
    format='price',
    to_wire=decimal_to_wire,
    to_python=decimal_str_to_python,
    validate=decimal_str_to_python,
    description=''
)
amount_format = SwaggerFormat(
    format='amount',
    to_wire=decimal_to_wire,
    to_python=decimal_str_to_python,
    validate=decimal_str_to_python,
    description=''
)
uuid_format = SwaggerFormat(
    format='uuid',
    to_wire=uuid_to_wire,
    to_python=uuid_str_to_python,
    validate=uuid_str_to_python,
    description=''
)

swagger_user_formats = (
    price_format,
    amount_format,
    uuid_format
)
