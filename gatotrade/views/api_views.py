import dataclasses
import logging

from pyramid.view import view_config

from gatotrade import exceptions
from gatotrade.models.product import Asset
from gatotrade.orders import OrderManager
from gatotrade.trades import TradeManager
from gatotrade.users import UserManager
from gatotrade.users import get_user_from_request

from . import httpexceptions


logger = logging.getLogger(__name__)


@view_config(route_name='api.users.create', request_method='POST',
             renderer='pyramid_swagger')
def create_user(request):
    users = UserManager(request.dbsession, request.registry)
    user_data = request.swagger_data['user']
    try:
        user, apikey_pair = users.add_user(user_data)
    except exceptions.UserExistsException:
        raise httpexceptions.UserExistsHTTPError

    response_data = user.to_dict()
    response_data['apikey'] = dataclasses.asdict(apikey_pair)
    request.response.status = 201

    return response_data


@view_config(route_name='api.users.accounts.get', request_method='GET',
             renderer='pyramid_swagger', permission='get_own_accounts')
def get_accounts(request):
    user = get_user_from_request(request)
    response_data = [account.to_dict() for account in user.accounts]

    return response_data


@view_config(route_name='api.users.accounts.deposit', request_method='POST',
             renderer='pyramid_swagger', permission='deposit_to_account')
def deposit_to_account(request):
    user = get_user_from_request(request)

    # NOTE: this is just for testing, simulates transfering funds from a bank
    # account to the "eur" account, so we have some funds for trading
    try:
        asset = Asset(request.matchdict['asset_id'])
        assert asset == Asset.eur, (
            'Currently only deposits to "eur" account are supported')
    except (AssertionError, ValueError):
        raise httpexceptions.InvalidAssetIdHTTPError()

    account = user.get_account_by_asset(asset)
    deposit_data = request.swagger_data['deposit']
    account.deposit(deposit_data['amount'])

    response_data = account.to_dict()
    request.response.status = 201

    return response_data


@view_config(route_name='api.orders', request_method='GET',
             renderer='pyramid_swagger', permission='get_own_orders')
def get_orders(request):
    user = get_user_from_request(request)
    order_manager = OrderManager(request.dbsession, request.registry)
    orders = order_manager.get_orders_for_user(user)
    response_data = [order.to_dict() for order in orders]

    return response_data


@view_config(route_name='api.orders', request_method='POST',
             renderer='pyramid_swagger', permission='create_order')
def create_order(request):
    user = get_user_from_request(request)
    order_data = request.swagger_data['order']
    product_id = order_data.pop('product_id')
    order_manager = OrderManager(request.dbsession, request.registry)

    try:
        order = order_manager.add_order(
            data=order_data,
            user=user,
            product_id=product_id,
        )
    except exceptions.InvalidProductIdException:
        raise httpexceptions.InvalidProductIdHTTPError
    except exceptions.CannotPlaceOrderException as exc:
        raise httpexceptions.CannotPlaceOrderHTTPError(str(exc))
    except exceptions.TradeWithSelfException:
        raise httpexceptions.TradeWithSelfHTTPError

    response_data = order.to_dict()
    request.response.status = 201

    return response_data


@view_config(route_name='api.orders.delete', request_method='DELETE',
             renderer='pyramid_swagger', permission='cancel_order')
def cancel_order(request):
    user = get_user_from_request(request)
    order_manager = OrderManager(request.dbsession, request.registry)

    try:
        order = order_manager.cancel_order_for_user(
            user=user, order_uuid=request.matchdict['order_id'])
    except exceptions.OrderNotFoundException:
        raise httpexceptions.InvalidOrderIdHTTPError
    except exceptions.CannotCancelOrderException:
        raise httpexceptions.CannotCancelOrderHTTPError

    response_data = order.to_dict()
    request.response.status = 200

    return response_data


@view_config(route_name='api.products.orderbook.get', request_method='GET',
             renderer='pyramid_swagger')
def get_order_book(request):
    product_id = request.matchdict['product_id']
    limit_order_books = request.registry.limit_order_books
    try:
        limit_order_book = limit_order_books.get_book(product_id)
    except exceptions.InvalidProductIdException:
        raise httpexceptions.InvalidProductIdHTTPError

    return limit_order_book.to_dict()


@view_config(route_name='api.products.trades.get', request_method='GET',
             renderer='pyramid_swagger')
def get_trades(request):
    product_id = request.matchdict['product_id']
    trade_manager = TradeManager(request.dbsession)

    try:
        trades = trade_manager.get_latest_trades_for_product(product_id)
    except exceptions.InvalidProductIdException:
        raise httpexceptions.InvalidProductIdHTTPError

    response_data = [trade.to_dict() for trade in trades]
    return response_data


def includeme(config):
    config.add_route('api.users.create', '/users')
    config.add_route('api.users.accounts.get', '/accounts')
    config.add_route(
        'api.users.accounts.deposit', '/accounts/{asset_id}/deposits')
    config.add_route('api.orders', '/orders')
    config.add_route('api.orders.delete', '/orders/{order_id}')
    config.add_route(
        'api.products.orderbook.get', '/products/{product_id}/book')
    config.add_route(
        'api.products.trades.get', '/products/{product_id}/trades')
