import logging

from pyramid.view import view_config
from pyramid.httpexceptions import HTTPClientError
from pyramid.httpexceptions import HTTPServerError
from pyramid_swagger.exceptions import RequestValidationError


logger = logging.getLogger(__name__)


@view_config(context=HTTPClientError, renderer='json')
def client_error(exc, request):
    request.response.status = exc.code
    error = {
        'type': exc.title,
        'message': exc.explanation
    }

    return error


@view_config(context=RequestValidationError, renderer='json')
def validation_error(exc, request):
    request.response.status = 400

    try:
        message = exc.child.message
    except AttributeError:
        message = str(exc.child)

    error = {
        'type': 'Validation Error',
        'message': message
    }

    return error


@view_config(context=Exception, renderer='json')
def unhandled_exception(exc, request):
    logger.exception('Unhandled exception')
    request.response.status = 500
    error = {
        'type': 'Server Error',
        'message': 'Internal server error.'
    }

    return error


@view_config(context=HTTPServerError, renderer='json')
def server_error(exc, request):
    logger.exception('Server error')
    request.response.status = exc.code
    error = {
        'type': exc.title,
        'message': exc.explanation
    }

    return error
