import transaction

from gatotrade.models import get_session_factory
from gatotrade.models import get_tm_session


def setup(env):
    session_factory = get_session_factory()

    with transaction.manager:
        env['dbsession'] = get_tm_session(session_factory, transaction.manager)
        env['tm'] = transaction.manager
