from decimal import Decimal
import base64
import dataclasses
import os
import sys

from pyramid.paster import get_appsettings
from pyramid.paster import setup_logging
from pyramid.scripts.common import parse_vars
from sqlalchemy.orm import sessionmaker
import transaction

from gatotrade.models import Product
from gatotrade.models import User
from gatotrade.models import get_engine
from gatotrade.models import get_tm_session
from gatotrade.models.meta import Base
from gatotrade.models.product import Asset
from gatotrade.models.product import get_available_assets
from gatotrade.models.user import ApiKeyPair
from gatotrade.users import UserManager


@dataclasses.dataclass
class ApiUser:
    user: User
    apikey_pair: ApiKeyPair

    @property
    def auth_headers(self):
        token = base64.b64encode(
            f'{self.apikey_pair.id}:{self.apikey_pair.secret}'.encode('utf-8')
        ).decode('utf-8')
        headers = f'Authorization: Basic {token}'
        return headers

    def to_dict(self):
        return {
            'email': self.user.email,
            'apikey_id': self.apikey_pair.id,
            'apikey_secret': self.apikey_pair.secret,
            'auth_headers': self.auth_headers
        }


class DummyRegistry:
    def __init__(self):
        self.events = []

    def notify(self, event):
        self.events.append(event)


def usage(argv):
    cmd = os.path.basename(argv[0])
    print('usage: %s <config_uri> [var=value]\n'
          '(example: "%s development.ini")' % (cmd, cmd))
    sys.exit(1)


def create_products(dbsession):
    omg_eth = Product(
        product_id='omg-eth',
        base_asset=Asset.omg,
        quote_asset=Asset.eth,
        min_amount=Decimal('0.001'),
        max_amount=Decimal('1000'),
    )
    eth_eur = Product(
        product_id='eth-eur',
        base_asset=Asset.eth,
        quote_asset=Asset.eur,
        min_amount=Decimal('0.001'),
        max_amount=Decimal('1000'),
    )
    dbsession.add(omg_eth)
    dbsession.add(eth_eur)


def create_users_with_funds(dbsession):
    INITIAL_ACCOUNT_BALANCE = 1000

    registry = DummyRegistry()
    user_manager = UserManager(dbsession, registry)
    users_data = [
        {
            'name': 'Scratchy',
            'surname': 'Cat',
            'email': 'scratchy.cat@example.com'
        },
        {
            'name': 'Happy',
            'surname': 'Cat',
            'email': 'happy.cat@example.com'
        }
    ]
    apiusers = []
    for user_data in users_data:
        user, apikey_pair = user_manager.add_user(user_data)
        apiuser = ApiUser(
            user=user,
            apikey_pair=apikey_pair
        )
        apiusers.append(apiuser.to_dict())
        for asset in get_available_assets():
            account = apiuser.user.get_account_by_asset(asset)
            account.deposit(INITIAL_ACCOUNT_BALANCE)
            dbsession.add(account)

    return apiusers


def populate_db(dbsession):
    create_products(dbsession)
    apiusers = create_users_with_funds(dbsession)
    return apiusers


def main(argv=sys.argv):
    if len(argv) < 2:
        usage(argv)
    config_uri = argv[1]
    options = parse_vars(argv[2:])
    setup_logging(config_uri)
    settings = get_appsettings(config_uri, options=options)

    engine = get_engine(settings)
    Base.metadata.create_all(engine)

    session_factory = sessionmaker()
    session_factory.configure(bind=engine)

    with transaction.manager:
        dbsession = get_tm_session(session_factory, transaction.manager)
        apiusers = populate_db(dbsession)

    print('Created %d users:\n' % len(apiusers))
    for apiuser in apiusers:
        for key, value in apiuser.items():
            print(f"{key}: '{value}'")
        print()
