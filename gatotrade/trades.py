import typing

from gatotrade.exceptions import InvalidProductIdException
from gatotrade.models import Product
from gatotrade.models import Trade


class TradeManager:
    def __init__(self, dbsession):
        self.dbsession = dbsession

    def get_latest_trades_for_product(
            self, product_id: str, limit: int = 500
    ) -> typing.Sequence[Trade]:
        # TODO: add index for product_id
        product = self.dbsession.query(Product).filter(
            Product.product_id == product_id).one_or_none()
        if product is None:
            raise InvalidProductIdException(product_id)

        trades = self.dbsession.query(Trade).join(Product).filter(
            Product.product_id == product_id).order_by(
                Trade.created.desc()).limit(limit).all()
        return trades
