from decimal import Decimal
import pytest

from gatotrade.matching.trading import TradeProcessor
from gatotrade.models.order import LimitOrder
from gatotrade.models.order import Order
from gatotrade.models.order import OrderStatus
from gatotrade.models.order import OrderSide
from gatotrade.models.product import Asset
from gatotrade.models.product import Product
from gatotrade.models.product import get_available_assets
from gatotrade.models.trade import LimitOrderTrade
from gatotrade.orders import OrderManager
from gatotrade.users import UserManager


INITIAL_ACCOUNT_BALANCE = 1000


@pytest.fixture
def product(dbsession):
    omg_eth = Product(
        product_id='omg-eth',
        base_asset='omg',
        quote_asset='eth',
        min_amount=Decimal('0.001'),
        max_amount=Decimal('1000'),
    )
    dbsession.add(omg_eth)
    return omg_eth


@pytest.fixture
def buyer(dbsession, config):
    """User which magically received some funds on the accounts, so he/she
    can start trading.
    """
    user_data = {
        'name': 'Grumpy',
        'surname': 'Cat',
        'email': 'grumpy.cat@example.com'
    }
    user_manager = UserManager(dbsession, config.registry)
    user, _ = user_manager.add_user(user_data)

    for asset in get_available_assets():
        account = user.get_account_by_asset(asset)
        account.deposit(INITIAL_ACCOUNT_BALANCE)
        dbsession.add(account)

    return user


@pytest.fixture
def seller(dbsession, config):
    """User which magically received some funds on the accounts, so he/she
    can start trading.
    """
    user_data = {
        'name': 'Happy',
        'surname': 'Cat',
        'email': 'happy.cat@example.com'
    }
    user_manager = UserManager(dbsession, config.registry)
    user, _ = user_manager.add_user(user_data)

    for asset in get_available_assets():
        account = user.get_account_by_asset(asset)
        account.deposit(INITIAL_ACCOUNT_BALANCE)
        dbsession.add(account)

    return user


@pytest.fixture
def orders(dbsession, product, buyer, seller):
    buy_order = Order(
        product=product,
        amount=Decimal('15'),
        price=Decimal('22.2'),
        side=OrderSide.BUY,
        user=buyer)
    sell_order = Order(
        product=product,
        amount=Decimal('10'),
        price=Decimal('22.1'),
        side=OrderSide.SELL,
        user=seller)
    dbsession.add(buy_order)
    dbsession.add(sell_order)

    return buy_order, sell_order


@pytest.fixture
def limit_order_trade(orders):
    buy_order, sell_order = orders
    limit_order_trade = LimitOrderTrade(
        amount=Decimal('10'),
        price=Decimal('22.1'),
        buy_order=LimitOrder.from_order(buy_order),
        sell_order=LimitOrder.from_order(sell_order),
    )
    return limit_order_trade


@pytest.fixture
def trade_processor(dbsession, config):
    order_manager = OrderManager(dbsession, config.registry)
    return TradeProcessor(dbsession, config.registry, order_manager)


class TestTradeProcessor:
    def test_process_trade(
            self, trade_processor, limit_order_trade, buyer, seller):
        # check initial account balances
        buyer_eth_account = buyer.get_account_by_asset(Asset.eth)
        buyer_omg_account = buyer.get_account_by_asset(Asset.omg)
        assert buyer_eth_account.balance == Decimal('1000')
        assert buyer_omg_account.balance == Decimal('1000')
        seller_eth_account = seller.get_account_by_asset(Asset.eth)
        seller_omg_account = seller.get_account_by_asset(Asset.omg)
        assert seller_eth_account.balance == Decimal('1000')
        assert seller_omg_account.balance == Decimal('1000')

        # process the trade
        trade = trade_processor.process_trade(limit_order_trade)

        # check the trade data
        assert trade.amount == Decimal('10')
        assert trade.price == Decimal('22.1')
        buy_order = trade.buy_order
        sell_order = trade.sell_order
        assert buy_order.status == OrderStatus.PARTIALLY_FILLED
        assert buy_order.remaining_amount == Decimal('5')
        assert sell_order.status == OrderStatus.FILLED

        # check account balances
        seller_fees = Decimal('10') * Decimal('22.1') * Decimal('0.001')
        buyer_fees = Decimal('10') * Decimal('0.001')
        # (bought 10 OMG for 221 ETH)
        buyer_eth_account = buyer.get_account_by_asset(Asset.eth)
        buyer_omg_account = buyer.get_account_by_asset(Asset.omg)
        assert buyer_eth_account.balance == Decimal('779')
        assert buyer_omg_account.balance == Decimal('1010') - buyer_fees
        # (sold 10 OMG for 221 ETH)
        seller_eth_account = seller.get_account_by_asset(Asset.eth)
        seller_omg_account = seller.get_account_by_asset(Asset.omg)
        assert seller_eth_account.balance == Decimal('1221') - seller_fees
        assert seller_omg_account.balance == Decimal('990')
