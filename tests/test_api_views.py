import base64
import dataclasses
import uuid

import pytest

from gatotrade.models import User
from gatotrade.models.product import get_available_assets
from gatotrade.models.user import ApiKeyPair
from gatotrade.users import UserManager


INITIAL_ACCOUNT_BALANCE = 1000


@dataclasses.dataclass
class ApiUser:
    user: User
    apikey_pair: ApiKeyPair

    @property
    def auth_headers(self):
        token = base64.b64encode(
            f'{self.apikey_pair.id}:{self.apikey_pair.secret}'.encode('utf-8')
        ).decode('utf-8')
        headers = {'Authorization': f'Basic {token}'}
        return headers


@pytest.fixture
def user_data():
    data = {
        'name': 'Grumpy',
        'surname': 'Cat',
        'email': 'grumpy.cat@example.com'
    }
    return data


@pytest.fixture
def user2_data():
    data = {
        'name': 'Happy',
        'surname': 'Cat',
        'email': 'happy.cat@example.com'
    }
    return data


@pytest.fixture
def user3_data():
    data = {
        'name': 'Scratchy',
        'surname': 'Cat',
        'email': 'scratchy.cat@example.com'
    }
    return data


@pytest.fixture
def apiuser1(dbsession, user_data, config):
    user_manager = UserManager(dbsession, config.registry)
    user, apikey_pair = user_manager.add_user(user_data)
    dbsession.flush()
    apiuser = ApiUser(
        user=user,
        apikey_pair=apikey_pair)

    return apiuser


@pytest.fixture
def apiuser1_with_funds(dbsession, content, apiuser1):
    """User which magically received some funds on the accounts, so he/she
    can start trading.
    """
    for asset in get_available_assets():
        account = apiuser1.user.get_account_by_asset(asset)
        account.deposit(INITIAL_ACCOUNT_BALANCE)
        dbsession.add(account)
    dbsession.flush()

    return apiuser1


@pytest.fixture
def apiuser2(dbsession, user2_data, config):
    user_manager = UserManager(dbsession, config.registry)
    user, apikey_pair = user_manager.add_user(user2_data)
    dbsession.flush()
    apiuser = ApiUser(
        user=user,
        apikey_pair=apikey_pair)

    return apiuser


@pytest.fixture
def apiuser2_with_funds(dbsession, content, apiuser2):
    """User which magically received some funds on the accounts, so he/she
    can start trading.
    """
    for asset in get_available_assets():
        account = apiuser2.user.get_account_by_asset(asset)
        account.deposit(INITIAL_ACCOUNT_BALANCE)
        dbsession.add(account)
    dbsession.flush()

    return apiuser2


@pytest.fixture
def apiuser3(dbsession, user3_data, config):
    user_manager = UserManager(dbsession, config.registry)
    user, apikey_pair = user_manager.add_user(user3_data)
    dbsession.flush()
    apiuser = ApiUser(
        user=user,
        apikey_pair=apikey_pair)

    return apiuser


@pytest.fixture
def apiuser3_with_funds(dbsession, content, apiuser3):
    """User which magically received some funds on the accounts, so he/she
    can start trading.
    """
    for asset in get_available_assets():
        account = apiuser3.user.get_account_by_asset(asset)
        account.deposit(INITIAL_ACCOUNT_BALANCE)
        dbsession.add(account)
    dbsession.flush()

    return apiuser3


@pytest.fixture
def invalid_auth_headers():
    token = base64.b64encode(b'invalid:credentials').decode('utf-8')
    headers = {'Authorization': f'Basic {token}'}
    return headers


@pytest.fixture
def order_data():
    data = {
        'product_id': 'eth-eur',
        'amount': '1.23',
        'price': '10.23',
        'side': 'buy'
    }
    return data


@pytest.fixture
def matching_orders_data():
    order1_data = {
        'product_id': 'eth-eur',
        'amount': '1.23',
        'price': '10.23',
        'side': 'buy'
    }
    order2_data = {
        'product_id': 'eth-eur',
        'amount': '1.23',
        'price': '10.23',
        'side': 'sell'
    }
    return order1_data, order2_data


@pytest.fixture
def buy_order_amount_too_high():
    data = {
        'product_id': 'eth-eur',
        'amount': '100000000',
        'price': '505.5',
        'side': 'buy'
    }
    return data


@pytest.fixture
def sell_order_amount_too_high():
    data = {
        'product_id': 'eth-eur',
        'amount': '100000000',
        'price': '505.5',
        'side': 'sell'
    }
    return data


@pytest.fixture
def orders_by_user(webtestapp, apiuser1_with_funds, apiuser2_with_funds):
    orders_data = [
        (
            apiuser1_with_funds,
            {
                'product_id': 'omg-eth',
                'amount': '1',
                'price': '10',
                'side': 'buy'
            }
        ),
        (
            apiuser2_with_funds,
            {
                'product_id': 'omg-eth',
                'amount': '2',
                'price': '12',
                'side': 'buy'
            },
        ),
    ]

    orders_by_user = {}
    for apiuser, order_data in orders_data:
        resp = webtestapp.post_json(
            '/orders',
            order_data,
            headers=apiuser.auth_headers,
            status=201)
        user = apiuser.user
        orders = orders_by_user.setdefault(user, [])
        order_data = resp.json
        orders.append(order_data)

    return orders_by_user


@pytest.fixture
def multiple_orders_smoke_data(
        apiuser1_with_funds, apiuser2_with_funds, apiuser3_with_funds):
    orders_data = [
        (
            apiuser1_with_funds,
            {
                'product_id': 'omg-eth',
                'amount': '1',
                'price': '10',
                'side': 'buy'
            }
        ),
        (
            apiuser2_with_funds,
            {
                'product_id': 'omg-eth',
                'amount': '2',
                'price': '12',
                'side': 'buy'
            },
        ),
        (
            apiuser1_with_funds,
            {

                'product_id': 'omg-eth',
                'amount': '1',
                'price': '9',
                'side': 'buy'
            },
        ),
        (
            apiuser3_with_funds,
            {
                'product_id': 'omg-eth',
                'amount': '10',
                'price': '11',
                'side': 'sell'
            },
        ),
        (
            apiuser3_with_funds,
            {
                'product_id': 'omg-eth',
                'amount': '1',
                'price': '10',
                'side': 'sell'
            },
        ),
        (
            apiuser1_with_funds,
            {
                'product_id': 'omg-eth',
                'amount': '1',
                'price': '14',
                'side': 'buy'
            },
        ),
        (
            apiuser2_with_funds,
            {
                'product_id': 'omg-eth',
                'amount': '3',
                'price': '11',
                'side': 'buy'
            },
        )
    ]
    return orders_data


class TestCreateUser:
    PATH = '/users'

    def test_invalid_data(self, webtestapp):
        resp = webtestapp.post_json(self.PATH, {'invalid': 'data'}, status=400)
        assert resp.json == {
            'type': 'Validation Error',
            'message': "'name' is a required property"
        }

    def test_success(self, webtestapp, user_data):
        resp = webtestapp.post_json(self.PATH, user_data)
        data = resp.json

        # test the dynamically generated values
        user_id = data.pop('id', None)
        try:
            uuid.UUID(user_id)
        except (TypeError, ValueError):
            pytest.fail(f'User id is not a UUID: {user_id}')
        apikey = data.pop('apikey', None)
        apikey_id = apikey.pop('id', None)
        apikey_secret = apikey.pop('secret', None)
        assert len(apikey_id) == 32
        assert len(apikey_secret) == 64
        assert not apikey

        # test the rest of the data
        assert data == {
            'name': 'Grumpy',
            'surname': 'Cat',
            'email': 'grumpy.cat@example.com',
            'bank_accounts': []
        }


class TestGetOrders:
    PATH = '/orders'

    def test_unauthenticated_is_not_allowed(self, webtestapp):
        resp = webtestapp.get(self.PATH, status=403)
        assert resp.json == {
            'type': 'Forbidden',
            'message': 'Access was denied to this resource.'
        }

    def test_has_no_orders(self, webtestapp, apiuser1):
        resp = webtestapp.get(
            self.PATH,
            headers=apiuser1.auth_headers,
            status=200)

        assert resp.json == []

    def test_has_orders(self, webtestapp, apiuser1, orders_by_user):
        user1_orders = orders_by_user.get(apiuser1.user)
        assert len(user1_orders) == 1  # sanity check
        expected_order_data = user1_orders[0]

        resp = webtestapp.get(
            self.PATH,
            headers=apiuser1.auth_headers,
            status=200)

        data = resp.json
        assert len(data) == 1
        actual_order_data = data[0]

        # test dynamically generated data
        created = actual_order_data.pop('created', None)
        assert created
        order_id = actual_order_data.pop('order_id', None)
        assert order_id == expected_order_data['order_id']

        # test the rest of the data
        assert actual_order_data == {
            'amount': '1.0000000000',
            'side': 'buy',
            'price': '10.0000000000',
            'product_id': 'omg-eth',
            'status': 'new'
        }


class TestCreateOrder:
    PATH = '/orders'

    def test_unauthenticated_is_not_allowed(self, webtestapp, order_data):
        resp = webtestapp.post_json(self.PATH, order_data, status=403)
        assert resp.json == {
            'type': 'Forbidden',
            'message': 'Access was denied to this resource.'
        }

    def test_invalid_credentials_is_not_allowed(
            self, webtestapp, invalid_auth_headers, order_data):
        resp = webtestapp.post_json(
            self.PATH, order_data, headers=invalid_auth_headers, status=403)
        assert resp.json == {
            'type': 'Forbidden',
            'message': 'Access was denied to this resource.'
        }

    def test_invalid_data(self, webtestapp, apiuser1):
        resp = webtestapp.post_json(
            self.PATH,
            {'invalid': 'data'},
            headers=apiuser1.auth_headers,
            status=400)

        assert resp.json == {
            'type': 'Validation Error',
            'message': "'product_id' is a required property"
        }

    def test_buy_order_amount_too_high(
            self, webtestapp, apiuser1_with_funds, buy_order_amount_too_high):
        resp = webtestapp.post_json(
            self.PATH,
            buy_order_amount_too_high,
            headers=apiuser1_with_funds.auth_headers,
            status=400)
        assert resp.json == {
            'type': 'Bad Request',
            'message': "Cannot place order, insufficient funds"
        }

    def test_sell_order_amount_too_high(
            self, webtestapp, apiuser1_with_funds, sell_order_amount_too_high):
        resp = webtestapp.post_json(
            self.PATH,
            sell_order_amount_too_high,
            headers=apiuser1_with_funds.auth_headers,
            status=400)
        assert resp.json == {
            'type': 'Bad Request',
            'message': "Cannot place order, insufficient funds"
        }

    def test_cannot_trade_with_self(
            self,
            webtestapp,
            apiuser1_with_funds,
            matching_orders_data,
    ):
        order1_data, order2_data = matching_orders_data

        # create the first order
        resp = webtestapp.post_json(
            self.PATH,
            order1_data,
            headers=apiuser1_with_funds.auth_headers,
            status=201)
        order_data = resp.json
        assert order_data['status'] == 'new'

        # second order is aborted since user cannot trade with self
        resp = webtestapp.post_json(
            self.PATH,
            order2_data,
            headers=apiuser1_with_funds.auth_headers,
            status=409)
        assert resp.json == {
            'type': 'Conflict',
            'message': 'Order aborted, cannot trade with self'
        }

    def test_error_when_creating_an_order_state_is_rolled_back(
            self,
            webtestapp,
            apiuser1_with_funds,
            apiuser2_with_funds,
            matching_orders_data,
            mocker
    ):
        order1_data, order2_data = matching_orders_data
        product_id = order1_data['product_id']

        # create the first order
        resp = webtestapp.post_json(
            self.PATH,
            order1_data,
            headers=apiuser1_with_funds.auth_headers,
            status=201)

        # order should be added to the order book
        resp = webtestapp.get(f'/products/{product_id}/book')
        orderbook_data = resp.json
        assert len(orderbook_data['buy_orders']) == 1
        assert len(orderbook_data['sell_orders']) == 0

        # when creating a second order an exception occurs when processing the
        # trades
        mocker.patch('gatotrade.orders.TradeProcessor.process_trade',
                     side_effect=Exception('Some exception'))
        resp = webtestapp.post_json(
            self.PATH,
            order2_data,
            headers=apiuser2_with_funds.auth_headers,
            status=500)
        assert resp.json == {
            'type': 'Server Error',
            'message': 'Internal server error.'
        }

        # there should be no trades and order book should be unmodified
        resp = webtestapp.get(f'/products/{product_id}/trades')
        assert resp.json == []
        resp = webtestapp.get(f'/products/{product_id}/book')
        orderbook_data2 = resp.json
        assert len(orderbook_data2['buy_orders']) == 1
        assert len(orderbook_data2['sell_orders']) == 0
        assert orderbook_data['buy_orders'][0]['order_id'] == orderbook_data2[
            'buy_orders'][0]['order_id']

    def test_create_order_success(
            self, webtestapp, apiuser1_with_funds, order_data):
        resp = webtestapp.post_json(
            self.PATH,
            order_data,
            headers=apiuser1_with_funds.auth_headers,
            status=201)

        data = resp.json

        # test the dynamically generated values
        order_id = data.pop('order_id', None)
        try:
            uuid.UUID(order_id)
        except (TypeError, ValueError):
            pytest.fail(f'Order id is not a UUID: {order_id}')
        # TODO: find a way to test the value
        created = data.pop('created', None)
        assert created

        # test the rest of the data
        assert data == {
            'product_id': 'eth-eur',
            'amount': '1.23',
            'price': '10.23',
            'side': 'buy',
            'status': 'new',
        }


class TestCancelOrder:
    PATH_TEMPLATE = '/orders/{order_id}'
    RANDOM_UUID = 'a375ab33-5b39-4dc3-9b5b-f822cfc55990'

    def test_unauthenticated_is_not_allowed(self, webtestapp):
        path = self.PATH_TEMPLATE.format(order_id=self.RANDOM_UUID)
        resp = webtestapp.delete(path, status=403)
        assert resp.json == {
            'type': 'Forbidden',
            'message': 'Access was denied to this resource.'
        }

    def test_invalid_order_id(self, webtestapp, apiuser1):
        path = self.PATH_TEMPLATE.format(order_id='invalid-order-id')
        resp = webtestapp.delete(
            path,
            headers=apiuser1.auth_headers,
            status=400)

        assert resp.json == {
            'type': 'Validation Error',
            'message': "'invalid-order-id' is not a 'uuid'"
        }

    def test_nonexisting_order_id(self, webtestapp, apiuser1):
        path = self.PATH_TEMPLATE.format(order_id=self.RANDOM_UUID)
        resp = webtestapp.delete(
            path,
            headers=apiuser1.auth_headers,
            status=404)

        assert resp.json == {
            'type': 'Not Found',
            'message': "Provided order id not found"
        }

    def test_cannot_cancel_order_from_another_user(
            self, webtestapp, apiuser1, apiuser2, orders_by_user):
        user2_orders = orders_by_user.get(apiuser2.user)
        assert len(user2_orders) == 1  # sanity check
        order_id = user2_orders[0]['order_id']

        path = self.PATH_TEMPLATE.format(order_id=order_id)
        resp = webtestapp.delete(
            path,
            headers=apiuser1.auth_headers,
            status=404)

        assert resp.json == {
            'type': 'Not Found',
            'message': "Provided order id not found"
        }

    def test_order_already_cancelled(
            self, webtestapp, apiuser1, orders_by_user):
        user1_orders = orders_by_user.get(apiuser1.user)
        assert len(user1_orders) == 1  # sanity check
        order_id = user1_orders[0]['order_id']

        path = self.PATH_TEMPLATE.format(order_id=order_id)
        resp = webtestapp.delete(
            path,
            headers=apiuser1.auth_headers,
            status=200)

        data = resp.json
        assert data['status'] == 'cancelled'

        # now try to cancel again
        resp = webtestapp.delete(
            path,
            headers=apiuser1.auth_headers,
            status=409)

        assert resp.json == {
            'type': 'Conflict',
            'message': 'Cannot cancel order in this state'
        }

    def test_cancel_order_success(self, webtestapp, apiuser1, orders_by_user):
        user1_orders = orders_by_user.get(apiuser1.user)
        assert len(user1_orders) == 1  # sanity check
        order_id = user1_orders[0]['order_id']

        path = self.PATH_TEMPLATE.format(order_id=order_id)
        resp = webtestapp.delete(
            path,
            headers=apiuser1.auth_headers,
            status=200)

        data = resp.json
        assert data['status'] == 'cancelled'


class TestSmoke:
    PRODUCT_ID = 'omg-eth'

    def test_smoke(
            self,
            webtestapp,
            multiple_orders_smoke_data,
            apiuser1_with_funds,
            apiuser2_with_funds,
            apiuser3_with_funds,
    ):
        # create some orders
        order_ids = []
        for apiuser, order_data in multiple_orders_smoke_data:
            assert order_data['product_id'] == self.PRODUCT_ID
            resp = webtestapp.post_json(
                '/orders',
                order_data,
                headers=apiuser.auth_headers,
                status=201)
            data = resp.json
            order_ids.append(data['order_id'])

        # check the returned data
        resp = webtestapp.get(f'/products/{self.PRODUCT_ID}/book')
        orderbook_data = resp.json
        assert orderbook_data == {
            'buy_orders': [
                # this order was not filled because the price is lower than all
                # sell orders
                {
                    'order_id': order_ids[2],
                    'amount': '1',
                    'remaining_amount': '1',
                    'price': '9',
                    'status': 'new'
                }
            ],
            'sell_orders': [
                # this order was partially filled
                {
                    'order_id': order_ids[3],
                    'amount': '10',
                    'remaining_amount': '4',
                    'price': '11',
                    'status': 'partially-filled'
                }

            ]
        }

        resp = webtestapp.get(f'/products/{self.PRODUCT_ID}/trades')
        trades_data = resp.json
        assert len(trades_data) == 4
        sorted_trades_data = sorted(
            trades_data, key=lambda x: x['created'], reverse=True)
        assert trades_data == sorted_trades_data
        trade4, trade3, trade2, trade1 = trades_data
        assert trade1['amount'] == '2.0000000000'
        assert trade1['price'] == '11.0000000000'
        assert trade2['amount'] == '1.0000000000'
        assert trade2['price'] == '10.0000000000'
        assert trade3['amount'] == '1.0000000000'
        assert trade3['price'] == '11.0000000000'
        assert trade4['amount'] == '3.0000000000'
        assert trade4['price'] == '11.0000000000'

        # now cancel one of the orders
        order_id = order_ids[2]
        resp = webtestapp.delete(
            f'/orders/{order_id}',
            order_data,
            headers=apiuser1_with_funds.auth_headers,
            status=200)
        data = resp.json
        assert data['order_id'] == order_id
        assert data['status'] == 'cancelled'

        # the cancelled buy order should be removed from the orderbook
        resp = webtestapp.get(f'/products/{self.PRODUCT_ID}/book')
        orderbook_data = resp.json
        assert orderbook_data == {
            'buy_orders': [],
            'sell_orders': [
                {
                    'order_id': order_ids[3],
                    'amount': '10',
                    'remaining_amount': '4',
                    'price': '11',
                    'status': 'partially-filled'
                }

            ]
        }

        # check account balances
        resp = webtestapp.get(
            f'/accounts',
            headers=apiuser1_with_funds.auth_headers,
            status=200)
        data = resp.json
        for account in data:
            account.pop('account_id')
        assert data == [
            {
                "asset": "eth",
                "balance": "979.0000000000"
            },
            {
                "asset": "eur",
                "balance": "1000.0000000000"
            },
            {
                "asset": "omg",
                "balance": "1001.9980000000"
            }
        ]
        resp = webtestapp.get(
            f'/accounts',
            headers=apiuser2_with_funds.auth_headers,
            status=200)
        data = resp.json
        for account in data:
            account.pop('account_id')
        assert data == [
            {
                "asset": "eth",
                "balance": "945.0000000000"
            },
            {
                "asset": "eur",
                "balance": "1000.0000000000"
            },
            {
                "asset": "omg",
                "balance": "1004.9950000000"
            }
        ]
        resp = webtestapp.get(
            f'/accounts',
            headers=apiuser3_with_funds.auth_headers,
            status=200)
        data = resp.json
        for account in data:
            account.pop('account_id')
        assert data == [
            {
                "asset": "eth",
                "balance": "1075.9240000000"
            },
            {
                "asset": "eur",
                "balance": "1000.0000000000"
            },
            {
                "asset": "omg",
                "balance": "993.0000000000"
            }
        ]
