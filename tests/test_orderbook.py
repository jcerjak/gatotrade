from decimal import Decimal
import datetime
import pytest
import uuid

from gatotrade.matching.orderbook import LimitOrderBook
from gatotrade.models.order import LimitOrder
from gatotrade.models.order import OrderSide
from gatotrade.models.order import OrderStatus

RANDOM_UUID = str(uuid.uuid4())
CREATED = datetime.datetime(2019, 12, 18, 13, 5)


@pytest.fixture
def order_book():
    orderbook = LimitOrderBook()
    return orderbook


@pytest.fixture
def buy_order_data():
    data = {
        'uuid': RANDOM_UUID,
        'created': CREATED,
        'amount': Decimal('10'),
        'price': Decimal('22.2'),
        'side': OrderSide.BUY,
        'status': 'new'
    }
    return data


@pytest.fixture
def sell_order_data():
    data = {
        'uuid': RANDOM_UUID,
        'created': CREATED,
        'amount': Decimal('10'),
        'price': Decimal('22.2'),
        'side': OrderSide.SELL,
        'status': 'new'
    }
    return data


@pytest.fixture
def buy_order(buy_order_data):
    buy_order = LimitOrder(**buy_order_data)
    return buy_order


@pytest.fixture
def sell_order(sell_order_data):
    sell_order = LimitOrder(**sell_order_data)
    return sell_order


@pytest.fixture
def non_matching_buy_and_sell_orders(buy_order_data, sell_order_data):
    buy_order_data['price'] = Decimal('22.0')
    buy_order = LimitOrder(**buy_order_data)

    sell_order_data['price'] = Decimal('22.1')
    sell_order = LimitOrder(**sell_order_data)

    return buy_order, sell_order


@pytest.fixture
def partially_matching_buy_and_sell_orders(buy_order_data, sell_order_data):
    buy_order_data['amount'] = Decimal('10')
    buy_order = LimitOrder(**buy_order_data)

    sell_order_data['amount'] = Decimal('7')
    sell_order = LimitOrder(**sell_order_data)

    return buy_order, sell_order


@pytest.fixture
def fully_matching_buy_and_sell_orders(buy_order_data, sell_order_data):
    buy_order = LimitOrder(**buy_order_data)
    sell_order = LimitOrder(**sell_order_data)

    return buy_order, sell_order


@pytest.fixture
def orders_smoke(buy_order_data, sell_order_data):
    buy_order_data['amount'] = Decimal('1')
    buy_order_data['price'] = Decimal('19.3')
    order1 = LimitOrder(**buy_order_data)

    buy_order_data['amount'] = Decimal('10')
    buy_order_data['price'] = Decimal('22.2')
    order2 = LimitOrder(**buy_order_data)

    sell_order_data['amount'] = Decimal('2')
    sell_order_data['price'] = Decimal('23.4')
    order3 = LimitOrder(**sell_order_data)

    buy_order_data['amount'] = Decimal('4')
    buy_order_data['price'] = Decimal('22.2')
    order4 = LimitOrder(**buy_order_data)

    sell_order_data['amount'] = Decimal('7')
    sell_order_data['price'] = Decimal('22.1')
    order5 = LimitOrder(**sell_order_data)

    sell_order_data['amount'] = Decimal('5')
    sell_order_data['price'] = Decimal('22.0')
    order6 = LimitOrder(**sell_order_data)

    orders = [order1, order2, order3, order4, order5, order6]
    return orders


class TestLimitOrderBook:
    def test_init(self, order_book):
        assert order_book.buy_orders == {}
        assert order_book.sell_orders == {}
        assert order_book.orders_by_uuid == {}
        assert order_book.trades == []

    def test__try_fill_orders_no_buy_orders_and_sell_orders(self, order_book):
        order_book._try_fill_orders()

        # no trade with empty order book
        assert order_book.trades == []

    def test_add_order_buy_no_sell_orders(self, order_book, buy_order):
        assert order_book.buy_orders == {}
        assert order_book.sell_orders == {}

        trades = order_book.add_order(buy_order)

        assert trades == []
        assert buy_order.status == OrderStatus.NEW
        assert order_book.buy_orders == {buy_order.price: [buy_order]}
        assert order_book.sell_orders == {}
        assert order_book.orders_by_uuid == {buy_order.uuid: buy_order}
        assert order_book.trades == []

    def test_add_order_sell_no_buy_orders(self, order_book, sell_order):
        assert order_book.buy_orders == {}
        assert order_book.sell_orders == {}

        trades = order_book.add_order(sell_order)

        assert trades == []
        assert sell_order.status == OrderStatus.NEW
        assert order_book.buy_orders == {}
        assert order_book.sell_orders == {sell_order.price: [sell_order]}
        assert order_book.orders_by_uuid == {sell_order.uuid: sell_order}
        assert order_book.trades == []

    def test_add_order_non_matching_orders(
            self, order_book, non_matching_buy_and_sell_orders):
        buy_order, sell_order = non_matching_buy_and_sell_orders
        trades = order_book.add_order(buy_order)
        assert trades == []
        trades = order_book.add_order(sell_order)
        assert trades == []

        # there is no trade since the order prices do not overlap
        assert not buy_order.is_filled
        assert buy_order.status == OrderStatus.NEW
        assert not sell_order.is_filled
        assert sell_order.status == OrderStatus.NEW
        assert order_book.buy_orders == {buy_order.price: [buy_order]}
        assert order_book.sell_orders == {sell_order.price: [sell_order]}
        assert order_book.orders_by_uuid == {
            buy_order.uuid: buy_order,
            sell_order.uuid: sell_order,
        }
        assert order_book.trades == []

    def test_add_order_partially_matching_orders(
            self, order_book, partially_matching_buy_and_sell_orders):
        buy_order, sell_order = partially_matching_buy_and_sell_orders
        trades = order_book.add_order(buy_order)
        assert trades == []
        trades = order_book.add_order(sell_order)

        assert trades == order_book.trades
        assert len(order_book.trades) == 1
        trade = order_book.trades[0]
        assert trade.amount == Decimal('7')
        assert trade.price == Decimal('22.2')
        assert trade.buy_order == buy_order
        assert trade.sell_order == sell_order

        # buy order should be partially filled, since the amount was to big
        # for the available sell order, while the sell order should be fully
        # filled
        assert not buy_order.is_filled
        assert buy_order.status == OrderStatus.PARTIALLY_FILLED
        assert buy_order.is_active
        assert buy_order.remaining_amount == Decimal('3')
        assert sell_order.is_filled
        assert not sell_order.is_active
        assert sell_order.remaining_amount == Decimal('0')

        assert order_book.buy_orders == {buy_order.price: [buy_order]}
        assert order_book.sell_orders == {sell_order.price: [sell_order]}
        assert order_book.orders_by_uuid == {
            buy_order.uuid: buy_order,
            sell_order.uuid: sell_order,
        }

    def test_add_order_fully_matching_orders(
            self, order_book, fully_matching_buy_and_sell_orders):
        buy_order, sell_order = fully_matching_buy_and_sell_orders
        trades = order_book.add_order(buy_order)
        assert trades == []
        trades = order_book.add_order(sell_order)

        assert trades == order_book.trades
        assert len(order_book.trades) == 1
        trade = order_book.trades[0]
        assert trade.amount == Decimal('10')
        assert trade.price == Decimal('22.2')
        assert trade.buy_order == buy_order
        assert trade.sell_order == sell_order
        assert buy_order.is_filled
        assert not buy_order.is_active
        assert buy_order.remaining_amount == Decimal('0')
        assert sell_order.is_filled
        assert not sell_order.is_active
        assert sell_order.remaining_amount == Decimal('0')

        assert order_book.buy_orders == {buy_order.price: [buy_order]}
        assert order_book.sell_orders == {sell_order.price: [sell_order]}
        assert order_book.orders_by_uuid == {
            buy_order.uuid: buy_order,
            sell_order.uuid: sell_order,
        }

    def test_add_order_sell_order_price_is_lower(
            self, order_book, buy_order_data, sell_order_data):
        buy_order_data['price'] = Decimal('22.2')
        buy_order = LimitOrder(**buy_order_data)

        sell_order_data['price'] = Decimal('19.1')
        sell_order = LimitOrder(**sell_order_data)

        trades = order_book.add_order(buy_order)
        assert trades == []
        trades = order_book.add_order(sell_order)

        # the trade is done at the sell order price
        assert trades == order_book.trades
        assert len(trades) == 1
        trade = order_book.trades[0]
        assert trade.amount == Decimal('10')
        assert trade.price == Decimal('19.1')
        assert trade.buy_order == buy_order
        assert trade.sell_order == sell_order
        assert buy_order.is_filled
        assert sell_order.is_filled

    def test_smoke(self, order_book, orders_smoke):
        for order in orders_smoke:
            order_book.add_order(order)

        order1, order2, order3, order4, order5, order6 = orders_smoke

        assert len(order_book.trades) == 3
        trade1 = order_book.trades[0]
        trade2 = order_book.trades[1]
        trade3 = order_book.trades[2]
        assert trade1.amount == Decimal('7')
        assert trade1.price == Decimal('22.1')
        assert trade1.buy_order == order2
        assert trade1.sell_order == order5
        assert trade2.amount == Decimal('3')
        assert trade2.price == Decimal('22.0')
        assert trade2.buy_order == order2
        assert trade2.sell_order == order6
        assert trade3.amount == Decimal('2')
        assert trade3.price == Decimal('22.0')
        assert trade3.buy_order == order4
        assert trade3.sell_order == order6

        assert order1.status == OrderStatus.NEW
        assert order2.status == OrderStatus.FILLED
        assert order3.status == OrderStatus.NEW
        assert order4.status == OrderStatus.PARTIALLY_FILLED
        assert order5.status == OrderStatus.FILLED
        assert order6.status == OrderStatus.FILLED
