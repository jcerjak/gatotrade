from decimal import Decimal

from pyramid import testing
from webtest import TestApp
import pytest
import transaction

from gatotrade import configure
from gatotrade import initdb
from gatotrade.models import get_tm_session
from gatotrade.models.meta import Base
from gatotrade.models import Product
from gatotrade.models.product import Asset


@pytest.fixture(scope='session')
def settings():
    return {
        'pyramid.includes': [
            'pyramid_swagger'
        ],
        'sqlalchemy.url': 'sqlite:///:memory:',
        'pyramid_swagger.schema_directory': 'gatotrade/views/schemas',
        'pyramid_swagger.schema_file': 'gatotrade.yaml',
    }


@pytest.yield_fixture
def config(settings):
    """Return a Pyramid `Configurator` object initialized
    with default (test) settings.
    """
    testing.setUp()
    config = configure({}, **settings)

    yield config

    testing.tearDown()


@pytest.yield_fixture
def dbsession(config):
    engine = config.registry['dbengine']
    Base.metadata.create_all(engine)
    session_factory = config.registry['dbsession_factory']
    session = get_tm_session(session_factory, transaction.manager)

    yield session

    transaction.abort()
    Base.metadata.drop_all(engine)


@pytest.fixture
def content(dbsession):
    omg_eth = Product(
        product_id='omg-eth',
        base_asset=Asset.omg,
        quote_asset=Asset.eth,
        min_amount=Decimal('0.001'),
        max_amount=Decimal('1000'),
    )
    eth_eur = Product(
        product_id='eth-eur',
        base_asset=Asset.eth,
        quote_asset=Asset.eur,
        min_amount=Decimal('0.001'),
        max_amount=Decimal('1000'),
    )
    dbsession.add(omg_eth)
    dbsession.add(eth_eur)
    dbsession.flush()
    # TODO: see if we can avoid committing the transaction here
    transaction.commit()


@pytest.fixture
def app(dbsession, content, config):
    initdb(config)
    app = config.make_wsgi_app()
    return app


@pytest.fixture
def webtestapp(app, content):
    return TestApp(app)
