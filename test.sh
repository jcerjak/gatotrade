#!/bin/bash

# exit on any error
set -e


echo "Running code style checks with flake8"
flake8 gatotrade/ tests/

echo "Running type checks with mypy"
mypy gatotrade --ignore-missing-imports

echo "Checking for vulnerable packages"
safety check

echo "Running tests"
py.test tests/ "$@"

echo "All checks ok."
